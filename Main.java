/** class Main, responsible for UI
@author d0ku (Jakub Piątkowski)
@version 1.0
*/

//jar -cvfe *.class

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.lang.*;
import java.io.*;
import java.applet.Applet;


class DisplayMenuListener implements ActionListener{
	GraphicalUserInterface inter;

	public void actionPerformed(ActionEvent e){
		
		switch (inter.which_menu.getSelectedItem()){

			case "Pascal print":
				inter.makePascalPrintUserInterface();
				break;
			case "RSADecrypt":
				inter.makeRSADecryptUserInterface();
				break;
			case "Pierwiastki":
				inter.makeRootUserInterface();
				break;
		}


	}
	DisplayMenuListener(GraphicalUserInterface inter){
		this.inter=inter;
	}
}


class UniversalTextFieldListener implements FocusListener{
	JTextField field;

	public void focusLost(FocusEvent e){field.setText(field.getText());}
	public void focusGained(FocusEvent e){field.setText("");}

	UniversalTextFieldListener(JTextField field){
		this.field=field;
	}
}

class RootTextFieldListener implements FocusListener{
	GraphicalUserInterface inter;

	public void focusGained(FocusEvent e){inter.root_input.setText("");}
	public void focusLost(FocusEvent e){inter.root_input.setText(inter.root_input.getText());}

	RootTextFieldListener(GraphicalUserInterface inter){
		this.inter=inter;
	} // unneeded , can be deleted
}

class RootButtonListener implements ActionListener{
	GraphicalUserInterface inter;
	String command;
	String arguments;
	double succes=0;
	String[] arr;
	char temporal;
	double trial;
	int degree;

	public void actionPerformed(ActionEvent e){

		arguments=inter.root_input.getText();

		switch(inter.root_choice.getSelectedItem()){
			case "Drugi":
				degree=2;
				break;
			case "Trzeci":
				degree=3;
				break;
			case "Czwarty":
				degree=4;
				break;
		}


		command="";
		arr=arguments.split(" ");

		for(int i=0;i<arr.length;i++){

			try{
				trial=Double.parseDouble(arr[i]);
			}
			catch(Exception exc){
				command=command+"!!!arg musi być liczbą!!! ";
				continue;
			}
			try{
				succes=Pierwiastek.stopien(trial,degree);
			}
			catch(LowerOrEqualZeroException ex){
				command=command+ "!!!arg<0!!! ";
			}

			command=command+succes+" ";
		}

		inter.root_input.setText("Policzone");
		inter.root_output.setText(command);
	}

	RootButtonListener(GraphicalUserInterface inter){
		this.inter=inter;
	}
}

class MainMenuReturnListener implements ActionListener{
	GraphicalUserInterface inter;

	public void actionPerformed(ActionEvent e){
		inter.returnToMainMenu();
	}
	MainMenuReturnListener(GraphicalUserInterface inter){
		this.inter=inter;
	}
}

class PascalPrintDisplayListener implements ActionListener{
	GraphicalUserInterface inter;

	public void actionPerformed(ActionEvent e){
		int verse_number;
		String verse;
		Boolean succes=true;


		for(int i=inter.pascal_print_menu.getComponentCount();i>3;i--){
			inter.pascal_print_menu.remove(i-1);
		}
		inter.pascal_print_menu.revalidate();
		inter.pascal_print_menu.repaint();
		inter.window.pack();
		try {verse_number=Integer.parseInt(inter.pascal_print_input.getText())+1;}
		catch(Exception ex){
			JTextField error = new JTextField("Wprowadź poprawny argument!!!");
			error.setEditable(false);
			inter.pascal_print_menu.add(error);
			System.out.println(ex);

			verse_number=-1;
			succes=false;
			
		}

		if(verse_number<0&&succes==true){
			JTextField verse_gui = new JTextField("Argument mniejszy od zera");
				verse_gui.setHorizontalAlignment(JTextField.CENTER);
				verse_gui.setEditable(false);
				inter.pascal_print_menu.add(verse_gui);

		}

		for (int i=0;i<verse_number;i++){

			WierszTrojkataPascala temp;
			try{temp = new WierszTrojkataPascala (i);
			}
			catch(Exception exception){
				for(int j=inter.pascal_print_menu.getComponentCount();j>3;j--){

					inter.pascal_print_menu.remove(j-1);
				}

				JTextField verse_gui = new JTextField(exception.getMessage());
				verse_gui.setHorizontalAlignment(JTextField.CENTER);
				verse_gui.setEditable(false);
				inter.pascal_print_menu.add(verse_gui);
				break;
			}

			try{
				verse=temp.wiersz();
				JTextField verse_gui = new JTextField(verse);
				verse_gui.setHorizontalAlignment(JTextField.CENTER);
				verse_gui.setEditable(false);
				Font font= new Font("SansSerif", Font.PLAIN, 8);
				if(i>=27)
					verse_gui.setFont(font);
				inter.pascal_print_menu.add(verse_gui);
				temp=null;}
			catch (Exception any_exception){
			}

		}
		inter.window.pack();



	}

	PascalPrintDisplayListener(GraphicalUserInterface inter){
		this.inter=inter;
	}
}

class RSADecryptDisplayListener implements ActionListener{
	GraphicalUserInterface inter;

	public void actionPerformed(ActionEvent e){
		String[] args = new String[3];
		String encoded=" ";
		Boolean succes=true;


		args[0]=inter.rsa_decrypt_input_private_1.getText();
		args[1]=inter.rsa_decrypt_input_private_2.getText();
		args[2]=inter.rsa_decrypt_input_text.getText();



		try{encoded=RSA_main.RSA_decrypt(args);}
		catch (RSAException ex_1){
			inter.rsa_decrypt_output.setText(ex_1.getMessage());
			succes=false;
		}
		catch (Exception ex_2){
			inter.rsa_decrypt_output.setText(ex_2.getMessage());
			succes=false;
		}

		if(succes==true){
			inter.rsa_decrypt_output.setText(encoded);
			inter.rsa_decrypt_output.setEditable(false);
			inter.rsa_decrypt_input_text.setText("Sukces");
		}

		inter.window.pack();

	}

	RSADecryptDisplayListener(GraphicalUserInterface inter){
		this.inter=inter;
	}

}
class GraphicalUserInterface{

	JInternalFrame  window;
	Button display_menu;
	JPanel main_menu;
	Button main_menu_return;
	JPanel pascal_print_menu;
	JTextField pascal_print_input;
	Button pascal_print_display;
	JPanel rsa_decrypt_menu;
	JTextField rsa_decrypt_input_private_1;
	JTextField rsa_decrypt_input_private_2;
	JTextField rsa_decrypt_input_text;
	Button rsa_decrypt_display;
	JTextField rsa_decrypt_output;
	JPanel root_menu;
	Choice root_choice;
	JTextField root_input;
	JTextField root_output;
	Button root_button;
	Choice which_menu;

	public void makeRSADecryptUserInterface(){
		rsa_decrypt_menu=new JPanel();
		rsa_decrypt_menu.setLayout(new FlowLayout());

		rsa_decrypt_input_private_1= new JTextField("Tu wprowadź 1 część klucza prywatnego");
		rsa_decrypt_input_private_1.addFocusListener(new UniversalTextFieldListener(rsa_decrypt_input_private_1));

		rsa_decrypt_input_private_2= new JTextField("Tu wprowadź 2 część klucza prywatnego");
		rsa_decrypt_input_private_2.addFocusListener(new UniversalTextFieldListener(rsa_decrypt_input_private_2));

		rsa_decrypt_input_text = new JTextField("Tu wprowadż zaszyfrowany tekst (':'' są traktowane jako przerwy pomiędzy znakami)");
		rsa_decrypt_input_text.addFocusListener(new UniversalTextFieldListener(rsa_decrypt_input_text));

		rsa_decrypt_display = new Button("Odkoduj");
		rsa_decrypt_display.addActionListener(new RSADecryptDisplayListener(this));

		rsa_decrypt_output= new JTextField("Tu pojawi się odkodowany tekst");

		rsa_decrypt_menu.add(main_menu_return);
		rsa_decrypt_menu.add(rsa_decrypt_input_private_1);
		rsa_decrypt_menu.add(rsa_decrypt_input_private_2);
		rsa_decrypt_menu.add(rsa_decrypt_input_text);
		rsa_decrypt_menu.add(rsa_decrypt_display);
		rsa_decrypt_menu.add(rsa_decrypt_output);
		

		window.add(rsa_decrypt_menu);
		main_menu_return.requestFocus();
		main_menu.setVisible(false);
		rsa_decrypt_menu.setVisible(true);
	
		window.pack();



	}
	public void makePascalPrintUserInterface(){
		pascal_print_menu=new JPanel();
		pascal_print_menu.setLayout(new BoxLayout(pascal_print_menu,BoxLayout.Y_AXIS));

		pascal_print_input= new JTextField("Tu wprowadź numer wiersza");
		pascal_print_input.addFocusListener(new UniversalTextFieldListener(pascal_print_input));

		pascal_print_display = new Button("Wyświetl");
		pascal_print_display.addActionListener(new PascalPrintDisplayListener(this));


		pascal_print_menu.add(main_menu_return);
		pascal_print_menu.add(pascal_print_input);
		pascal_print_menu.add(pascal_print_display);
		

		window.add(pascal_print_menu);
		main_menu_return.requestFocus();
		main_menu.setVisible(false);
		pascal_print_menu.setVisible(true);

		window.pack();



	}

	public void returnToMainMenu(){
		if(rsa_decrypt_menu!=null){
			rsa_decrypt_menu.setVisible(false);
		}
		if(pascal_print_menu!=null){
			pascal_print_menu.setVisible(false);
			
		}
		if(root_menu!=null){
			root_menu.setVisible(false);
			
		}

		main_menu.setVisible(true);
		window.remove(main_menu);
		window.add(main_menu);
		window.revalidate();
		window.repaint();
		window.pack();
		
	}

	public void makeRootUserInterface(){
		root_menu=new JPanel();

		root_choice= new Choice();
		root_choice.add("Drugi");
		root_choice.add("Trzeci");
		root_choice.add("Czwarty");

		root_input= new JTextField("Tu wprowadź argumenty", 20);
		root_input.addFocusListener(new UniversalTextFieldListener(root_input));

		root_button=new Button("Policz");
		root_button.addActionListener(new RootButtonListener(this));


		root_output=new JTextField("Tu pojawią sią wyniki", 20);

		root_menu.add(main_menu_return);//adds return to main menu button
		root_menu.add(root_choice);
		root_menu.add(root_input);
		root_menu.add(root_button);
		root_menu.add(root_output);



		window.add(root_menu);
		main_menu_return.requestFocus();
		main_menu.setVisible(false);
		root_menu.setVisible(true);
		
		window.pack();
		

	}

	public JInternalFrame getFrameField(){
		return this.window;
	}

	GraphicalUserInterface(){
		window = new JInternalFrame ("All in One by d0ku");
		window.setBounds(600,400,400,600);
		
		window.setDefaultCloseOperation(JInternalFrame .EXIT_ON_CLOSE);

		main_menu= new JPanel();
		main_menu.setLayout(new FlowLayout());
		main_menu.setVisible(true);


		which_menu= new Choice();
		which_menu.add("Pascal print");
		which_menu.add("Pierwiastki");
		which_menu.add("RSADecrypt");

		main_menu_return= new Button("Powrót");
		main_menu_return.addActionListener(new MainMenuReturnListener(this));


		display_menu= new Button("Wyświetl menu");
		display_menu.addActionListener(new DisplayMenuListener(this));

		window.add(main_menu);
		main_menu.add(which_menu);
		main_menu.add(display_menu);
		window.pack();
		window.setVisible(true);

	}


}

public class Main extends JApplet{

	public static void main(String[] args) {
		GraphicalUserInterface window= new GraphicalUserInterface();
		JFrame frame = new JFrame();
		frame.setBounds(600,400,400,600);
		frame.add(window.getFrameField());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

		
	}

	public void init(){
		GraphicalUserInterface window= new GraphicalUserInterface();
		this.setVisible(true);
		TextField xd = new TextField("XDXDXD");
		this.add(window.getFrameField());
		//this.add(window.window);
	}
}